A series of R functions and algoritms to analyze and visualize changes in power of EEG frequencies recorded using the [INTAN SYSTEM](http://intantech.com/RHD2000_evaluation_system.html). The data are initally imported and analyzed in Python (with Alessandro's script).
Functions are grouped in function_files;
Scripts in script_files.
